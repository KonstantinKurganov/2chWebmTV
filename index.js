/*
 * 2chWebm parser 
 * Mainn server file
 */

//Dependences
const express = require('express');
const app = express();
const dvachService = require('./services/2ch-api-service');

//Settings
const port = process.env.PORT || 3000;

app.set('view engine', 'pug');
app.set('views', './views');
app.use(express.static('public'));

//Routes
app.get('/threads', function (req, res) {
    dvachService.getAllWebmFromThread(req.query.threadId)
        .then((webms)=>{
            res.send(webms)
        })    
  });

app.get('/', function (req, res) {
    dvachService.getWebmThreads()
        .then((webmThreads)=>{
            for(let t of webmThreads){
                if (t.subject.length>63)
                    t.subject=t.subject.slice(0,63);
            }
            dvachService.getAllWebmFromThread(webmThreads[0].num)
                .then((webms)=>{
                    res.render('index',{webms:webms,webmThreads:webmThreads})
                })
        })
  });

app.listen(port, function () {
    console.log('2ch app listening on port '+port);
});